describe('Test surveyApp', function(){
    var scope;
    var config = {
      apiKey: "AIzaSyBDBeP25YSh1I3QzypJt3Pn6M30w0OSIvE",
      authDomain: "survey-ebf96.firebaseapp.com",
      databaseURL: "https://survey-ebf96.firebaseio.com",
      storageBucket: "survey-ebf96.appspot.com",
      messagingSenderId: "105965164482"
    };
    firebase.initializeApp(config);

    beforeEach(angular.mock.module('surveyApp'));

    beforeEach(angular.mock.inject(function(_$controller_){
      $controller = _$controller_;
    }));


    describe('Test LoginCtrl', function () {
        beforeEach(angular.mock.inject(function($rootScope, $controller){
            scope = $rootScope.$new();
            $controller('LoginCtrl', {$scope: scope});
        }));

        it('should have loginRole as "User"', function(){
            expect(scope.select.loginRole).toBe('User');
        });
    });

    describe('Test AdminCtrl', function () {

        localStorage['survey'] = JSON.stringify({
            title: 'Test Survey',
            description: 'Test Description',
            active: true,
            when: '123456',
            questions: [],
        });

        var randomSurveyTitle = Date.now().toString();

        var testSurveyData = {
            title: 'Test Survey',
            description: 'Test Description',
            active: true,
            when: '123456',
            questions: [
                {type: 'text', desc: ''},
                {type:'multiple', desc: '', options: ['']}
            ],
            locked: false,
        };

        beforeEach(angular.mock.inject(function($rootScope, $controller){
            $rootScope.userData = {
                email: 'test@user.com',
                displayName: 'Test User',
                photoURL: 'photo',
                uid: '123',
                role: 'admin',
            };
            scope = $rootScope.$new();
            $controller('AdminCtrl', {$scope: scope});
        }));

        it('should fetch the survey object from localStorage', function(){
            expect(scope.survey.title).toBe('Test Survey');
            expect(scope.survey.description).toBe('Test Description');
            expect(scope.survey.active).toBeTruthy();
            expect(scope.survey.when).toBe('123456');
            expect(scope.survey.questions.length).toBe(0);
        });

        it('should be able to add questions to the survey', function(){
            //spyOn(scope, 'updateSurveyToCache').and.callThrough();
            scope.addSurveyQuestion('text');
            expect(scope.survey.questions.length).toBe(1);
            scope.addSurveyQuestion('multiple');
            expect(scope.survey.questions.length).toBe(2);
        });

        it('should be able set a survey object to preview', function(){
            expect(angular.equals({}, scope.surveyToPreview)).toBeTruthy();
            scope.setSurveyToPreview(scope.survey, false);
            expect(angular.equals(testSurveyData, scope.surveyToPreview)).toBeTruthy();
            testSurveyData.locked = true;
            scope.setSurveyToPreview(scope.survey, true);
            expect(angular.equals(testSurveyData, scope.surveyToPreview)).toBeTruthy();
        });

        it('should be able to remove a survey question', function(){
            expect(scope.survey.questions.length).toBe(2);
            scope.removeSurveyQuestion(0);
            expect(scope.survey.questions.length).toBe(1);
        });

        it('should be able to add an option to a survey question', function(){
            expect(scope.survey.questions[0].options.length).toBe(1);
            scope.addSurveyQuestionOption(0);
            expect(scope.survey.questions[0].options.length).toBe(2);
        });

        it('should be able to remove an option from a survey question', function(){
            expect(scope.survey.questions[0].options.length).toBe(2);
            scope.removeSurveyQuestionOption(0, 1);
            expect(scope.survey.questions[0].options.length).toBe(1);
            scope.removeSurveyQuestionOption(0, 0);
            expect(scope.survey.questions.length).toBe(0);
        });

        it('should be able update a survey to cache', function(){
            scope.survey.questions = [{type: 'text', desc: ''}];
            expect(angular.equals(JSON.parse(localStorage.survey), scope.survey)).toBeFalsy();
            scope.updateSurveyToCache();
            expect(angular.equals(JSON.parse(localStorage.survey), scope.survey)).toBeTruthy();
        });

        it('should be able update a survey to DB', function(){
            spyOn(scope, 'updateSurveyToCache');
            scope.updateSurveyToDB();
            expect(scope.survey.title).toBe('');
            expect(scope.updateSurveyToCache).toHaveBeenCalled();
        });

        it('should be able to edit a survey', function(){
            scope.surveys = [{title: 'test1'}, {title: 'test2'}];
            expect(scope.survey.title).toBe('Test Survey');
            spyOn(scope, 'updateSurveyToDB');
            scope.editSurvey(1, 'mode');
            expect(scope.updateSurveyToDB).toHaveBeenCalled();
            expect(scope.survey.title).toBe('test2');
        });

        it('should return time passed from now', function(){
            expect(scope.getTime(Date.now())).toBe('a few seconds ago');
        });
    });

    describe('Test UserCtrl', function () {

        var testSurveyData = {
            title: 'Test Survey',
            description: 'Test Description',
            active: true,
            when: '123456',
            questions: [
                {type: 'text', desc: 'Test Qus1', answers: 'ans1'},
                {type:'multiple', options: ['op1', 'op2']}
            ],
            locked: false,
        };

        beforeEach(angular.mock.inject(function($rootScope, $controller){
            $rootScope.userData = {
                email: 'test@user.com',
                displayName: 'Test User',
                photoURL: 'photo',
                uid: '123',
                role: 'admin',
            };
            scope = $rootScope.$new();
            $controller('UserCtrl', {$scope: scope});
        }));

        it('should check if the survey has already been sent', function(){
            scope.surveyResponse = {'c3VydmV5MQ==':{title:'survey1'}};
            expect(scope.surveyAlreadySent('survey1')).toBe(true);
        });

        it('should validate survey response before sending', function(){
            spyOn(scope, 'updateSurveyResToDB');
            scope.validateAndSend(testSurveyData)
            expect(scope.updateSurveyResToDB).not.toHaveBeenCalled();
            testSurveyData.questions[1].answers = [true, false];
            scope.validateAndSend(testSurveyData)
            expect(scope.updateSurveyResToDB).toHaveBeenCalled();
        });

        it('should set a survey response to preview', function(){
            scope.surveys = [{title: 'survey1'}, {title: 'survey2'}];
            scope.surveyResponse = {'c3VydmV5MQ==':{title:'survey1'}};
            scope.setSurveyToPreview(0, 'survey1');
            expect(scope.surveyToPreview.title).toBe('survey1');
            scope.setSurveyToPreview(1, 'survey2');
            expect(scope.surveyToPreview.title).toBe('survey2');
        });

        it('should return time passed from now', function(){
            expect(scope.getTime(Date.now())).toBe('a few seconds ago');
        });
    });
});
