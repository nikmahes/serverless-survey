angular.module('surveyApp.controllers', [])

.controller('LoginCtrl', function($scope, $rootScope, $firebaseAuth, $state) {
  console.log("in LoginCtrl !");

  $scope.database = firebase.database();
  $scope.select = {
    loginRole:'User',
  };

  $scope.signInParams = {
    email: "",
    password: "",
  };

  $scope.commonLogin = function(serviceProvider) {
    console.log('In commonLogin !');
    var provider, clientId, clientSecret;
    if (serviceProvider == 'facebook') {
      console.log('Processing facebook login !');
      provider = firebase.auth.FacebookAuthProvider;
      clientId = '221518794977085';
    } else if (serviceProvider == 'google') {
      console.log('Processing google login !');
      provider = firebase.auth.GoogleAuthProvider;
      clientId = '105965164482-tuttba8cvsmugu3phph12grjasno6vbi.apps.googleusercontent.com';
    } else if (serviceProvider == 'twitter') {
      console.log('Processing twitter login !');
      provider = firebase.auth.TwitterAuthProvider;
      clientId = 'fu3L65EGIfIwL2jAAh4oIk1qJ';
      clientSecret = 'lmthm9S7AVo7cuJ21MohFVs9QPQxkdV9yTc2Zk5QTWK8ysBNbh';
    }
    $scope.oauthLogin(provider, clientId, clientSecret);
  };

  $scope.oauthLogin = function(provider, clientId, clientSecret) {
    console.log('In oauthLogin !');
    $firebaseAuth(firebase.auth().signInWithPopup(new provider()).then(function(result) {
      console.log('result');
      console.log(result);
      $rootScope.userData = {
        email: result.user.email,
        displayName:result.user.displayName,
        photoURL: result.user.photoURL,
        uid: result.user.uid,
        role: $scope.select.loginRole.toLocaleLowerCase(),
      };
      $scope.updateUserToDB($rootScope.userData);
    }).catch(function(error) {
      alert(error)
      console.log(error);
    }));
  };

  $scope.updateUserToDB = function(user) {
    console.log('in updateUserToDB !');
    console.log(user);
    //localStorage.setItem('AudRevUserData', JSON.stringify(user));
    $scope.database.ref('users/' + user.uid).set(user);
    $state.go('admin');
  };
})

.controller('AdminCtrl', function($scope, $rootScope) {
  console.log("in AdminCtrl !");

  $scope.database = firebase.database();
  $scope.surveys = [];
  $scope.surveyResponse = [];
  $scope.surveyToPreview = {};
  $scope.serveyResponseTitle = '';

  $scope.survey = {
    title: '',
    description: '',
    active: true,
    when: '',
    questions: [],
  };

  $scope.survey = localStorage['survey'] ? JSON.parse(localStorage['survey']) : $scope.survey;

  $scope.addSurveyQuestion = function(type) {
    console.log('addSurveyQuestion called !');
    if(type == 'text') {
      $scope.survey.questions.push({
        type: 'text',
        desc: '',
      });
    } else if(type == 'multiple') {
      $scope.survey.questions.push({
        type:'multiple',
        desc: '',
        options: [''],
      });
    }
    $scope.updateSurveyToCache();
  };

  $scope.setSurveyToPreview = function(survey, locked) {
    console.log('setSurveyToPreview called !');
    survey.locked = locked;
    $scope.surveyToPreview = survey;
  };

  $scope.removeSurveyQuestion = function(index) {
    console.log('removeSurveyQuestion called !');
    $scope.survey.questions.splice(index, 1);
    $scope.updateSurveyToCache();
  };

  $scope.addSurveyQuestionOption = function(index) {
    console.log('addSurveyQuestionOption called !');
    $scope.survey.questions[index].options.push('');
    $scope.updateSurveyToCache();
  };

  $scope.removeSurveyQuestionOption = function(parentIndex, index){
    console.log('removeSurveyQuestionOption called !');
    if (index == 0) {
      $scope.removeSurveyQuestion(parentIndex);
    } else {
      $scope.survey.questions[parentIndex].options.splice(index, 1);
    }
    $scope.updateSurveyToCache();
  };

  $scope.updateSurveyToCache = function() {
    console.log('updateSurveyToCache called !');
    localStorage.survey = JSON.stringify($scope.survey);
  };
  
  $scope.updateSurveyToDB = function() {
    console.log('updateSurveyToDB called !');
    $scope.survey.when = Date.now();
    var survey = angular.copy($scope.survey);
    $scope.survey = {
      title: '',
      description: '',
      active: true,
      when: '',
      questions: [],
    };
    $scope.updateSurveyToCache();
    if(survey['title'] && survey['title'].trim() != '') {
      $scope.database.ref('surveys/' + btoa(survey.title)).set(survey);
    }
  };

  $scope.fetchSurveysFromDB = function() {
    console.log('fetchSurveysFromDB called !');

    if($scope.surveys.length != 0) return;

    var surveysRef = $scope.database.ref('surveys/');
    surveysRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var snapshotVal = snapshot.val();
        $scope.surveys = Object.keys(snapshotVal).map(function (key) {
          return snapshotVal[key];
        });
        console.log($scope.surveys);
      });
    });
  };

  $scope.fetchSurveyResFromDB = function(surveyTitle, mode) {
    console.log('fetchSurveyResFromDB called !');

    $scope.serveyResponseTitle = surveyTitle;
    var surveyResRef = $scope.database.ref('surveyResponse/' + btoa(surveyTitle));
    surveyResRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var snapshotVal = snapshot.val();
        if(snapshotVal == null) snapshotVal = {};
        $scope.surveyResponse = Object.keys(snapshotVal).map(function (key) {
          snapshotVal[key].by = atob(key);
          return snapshotVal[key];
        });
        console.log($scope.surveyResponse);
      });
    });
    $('ul.tabs').tabs('select_tab', mode);
  };

  $scope.editSurvey = function(index, mode) {
    console.log('editSurvey called !');
    $scope.updateSurveyToDB();
    $scope.survey = $scope.surveys[index];
    $scope.updateSurveyToCache();
    if(typeof($) !== 'undefined') {
      $('ul.tabs').tabs('select_tab', mode);
    }
  };

  // $scope.surveyAlreadySent = function(surveyTitle) {
  //   console.log('surveyAlreadySent called !');
  //   return $scope.surveyResponse[btoa(surveyTitle)] ? true : false;
  // };

  $scope.deleteSurvey = function(index) {
    console.log('deleteSurvey called !');
    var survey = $scope.surveys[index];
    $scope.database.ref('surveys/' + btoa(survey.title)).set({});
  };

  $scope.getTime = function(timestamp) {
    console.log('safeApply called !');
    return moment(timestamp).fromNow();
  };

  $scope.safeApply = function(fn) {
    console.log('safeApply called !');
    if (this.$root) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
          if (fn && (typeof (fn) === 'function')) {
            fn();
          }
      } else {
          this.$apply(fn);
      }
    }
  };
})

.controller('UserCtrl', function($scope, $rootScope) {
  console.log("in UserCtrl !");

  $scope.database = firebase.database();
  $scope.surveys = [];
  $scope.surveyResponse = {};

  $scope.fetchSurveysFromDB = function() {
    console.log('fetchSurveysFromDB called !');

    if($scope.surveys.length != 0) return;

    var surveysRef = $scope.database.ref('surveys/');
    surveysRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var snapshotVal = snapshot.val();
        $scope.surveys = Object.keys(snapshotVal).map(function(key) {
          return snapshotVal[key];
        });
        console.log($scope.surveys);
      });
    });
  };

  $scope.fetchSurveyResFromDB = function() {
    console.log('fetchSurveyResFromDB called !');

    Object.keys(localStorage).forEach(function(key){
      if(key.indexOf('firebase:authUser') == 0) {
        $scope.safeApply(function(){
          $scope.userEmail = JSON.parse(localStorage[key]).email;
        });
      }
    })

    console.log($scope.userEmail);

    var surveyResRef = $scope.database.ref('surveyResponse/'  + btoa($scope.userEmail));
    surveyResRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var snapshotVal = snapshot.val();
        if(snapshotVal == null) snapshotVal = {};
        $scope.surveyResponse = snapshotVal;
        console.log('$scope.surveyResponse');
        console.log($scope.surveyResponse);
      });
      $scope.fetchSurveysFromDB();
    });
  };

  $scope.validateAndSend = function(survey) {
    console.log('validateAndSend called !');
    var valid = true;
    var alertShown = false;
    survey.questions.forEach(function(question, index){
      if(question.type == 'text') {
        valid = valid && question.answers && question.answers.trim().length > 0 ? true : false;
      } else if (question.type == 'multiple') {
        valid = valid && question.answers && Object.keys(question.answers).filter(function(v){
          return question.answers[v]
        }).length ? true : false;
      }

      if(!valid && !alertShown) {
        alert('Please record the answer of question no.' + (index + 1));
        alertShown = true;
        return false;
      }
    });
    console.log(valid);
    if(valid) $scope.updateSurveyResToDB(survey);
  };

  $scope.updateSurveyResToDB = function(survey) {
    console.log('updateSurveyResToDB called !');
    survey.when = Date.now();
    var userEmail = $rootScope.userData.email;
    var surveyRes = angular.copy(survey);
    $scope.database.ref('surveyResponse/' + btoa(surveyRes.title) + '/' + btoa(userEmail)).set(surveyRes);
    $scope.database.ref('surveyResponse/' + btoa(userEmail) + '/' + btoa(surveyRes.title)).set(surveyRes);
  };

  $scope.surveyAlreadySent = function(surveyTitle) {
    console.log('surveyAlreadySent called !');
    return $scope.surveyResponse[btoa(surveyTitle)] ? true : false;
  }

  $scope.setSurveyToPreview = function(index, surveyTitle) {
    console.log('setSurveyToPreview called !');
    if($scope.surveyAlreadySent(surveyTitle)) {
      $scope.surveyToPreview = $scope.surveyResponse[btoa(surveyTitle)];
    } else {
      $scope.surveyToPreview = $scope.surveys[index];
    }
  };

  $scope.getTime = function(timestamp) {
    console.log('safeApply called !');
    return moment(timestamp).fromNow();
  };

  $scope.safeApply = function(fn) {
    console.log('safeApply called !');
    if (this.$root) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
          if (fn && (typeof (fn) === 'function')) {
            fn();
          }
      } else {
          this.$apply(fn);
      }
    }
  };
});