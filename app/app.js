angular.module('surveyApp', [
  'ui.router',
  'ui.materialize',
  'firebase',
  'surveyApp.services',
  'surveyApp.controllers',
])
.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");
  $stateProvider
    .state('login', {
      url: "/",
      templateUrl: "templates/login.html",
      controller: "LoginCtrl"
    })
    .state('admin', {
      url: "/admin",
      templateUrl: "templates/admin.html",
      controller: "AdminCtrl"
    })
    .state('user', {
      url: "/user",
      templateUrl: "templates/user.html",
      controller: "UserCtrl"
    });
})

.run(function ($rootScope, $state) {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log('User is signed in !');
      firebase.database().ref('users/' + user.uid).on('value', function(snapshot) {
          var user = snapshot.val();
          console.log(user);
          $rootScope.userData = {
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            uid: user.uid,
            role: user.role,
          };
          $state.go(user.role);
      });
    } else {
      console.log('No user is signed in !');
    }
  });

  $rootScope.logout = function() {
    console.log('logout called !');
      firebase.auth().signOut().then(function() {
      console.log('Sign-out successful');
      $state.go('login');
    }, function(error) {
      console.log('error in log out');
    })
  };
});
