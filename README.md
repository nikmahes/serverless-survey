# About

A serverless survey application using AngularJS, MaterializeCSS and Firebase RealtimeDB.

# Local Installation and Execution

1. Install dependencies with npm 
    $ npm install

2. Run Application
    $ npm start

3. Open http://localhost:8080 in browser

4. Test Application
    $ karma start

# Todo(s)

1. ~~Create a Project Base (Node, Express, Gulp, AngularJS, AngularFire, MaterializeCSS)~~
2. ~~Add Test Runner & Write Test Scenarios (Karma, Jasmine)~~
3. ~~Create a Firebase Project and Integrate Social Login (Google / Facebook / Twitter) using Firebse Auth~~
4. ~~Create Admin / User Login Screens and Verify Person as Admin / User~~
5. Admin Features :
• ~~See a list of existing surveys
• Create/edit/delete a survey
• Add/delete/edit questions to a survey – choice and text based.
• For each survey, see a list of survey responses from the normal users.~~
6. User Features :
• ~~See the list of existing surveys.
• Take a survey (ie. enter answers for all the questions and submit it)
• See appropriate validation messages. eg: if he enters a non-numeric value for a numeric question or forgets to answer one of the questions~~

# Demo

https://survey-ebf96.firebaseapp.com

# Author

Nikhil Maheshwari (http://www.nikhilmaheshwari.com)